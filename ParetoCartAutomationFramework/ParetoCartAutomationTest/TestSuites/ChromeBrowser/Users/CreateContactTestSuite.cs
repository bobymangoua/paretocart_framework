﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParetoCartAutomationFramework.TestClasses.Users;
using ParetoCartAutomationTest.Core;

/*
 * @author: lmangoua
 * Date: 09/07/2018
 * */

namespace ParetoCartAutomationTest
{
    [TestClass]
    public class CreateContactTestSuite : BaseClass //To inherit from BaseClass (Same as doing "LoginTestSuite extends BaseClass {} in java")
    {
        //Loging to Page
        [TestMethod]
        public void Create_Contact()
        {
            //Login to ParetoCart
            NavigateToURLAndLoginTestSuite.LoginToParetoCart();

            //Select Users option
            CreateContact.SelectUsers();

            //Select Users_Contacts option
            CreateContact.SelectUsers_Contacts();

            //Add Contact
            CreateContact.AddContact();

            //Enter Detail
            CreateContact.EnterDetail();

            //Validate contact is created
            CreateContact.ValidateContactCreated();

        }
    }
}
