﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * @author: lmangoua
 * Date: 09/07/2018
 * */

namespace ParetoCartAutomationFramework.PageObjects.Users
{
    public static class CreateContactPageObjects
    {
        #region Icons
        public static string HomeIconXpath()
        {
            return "//a[@id='37']//i";
        }
        #endregion

        #region Button
        #endregion 

        #region Textbox
        public static string FirstNameTextBoxXpath()
        {
            return "//label[text()='First Name ']/../..//input";
        }

        public static string LastNameTextBoxXpath()
        {
            return "//label[text()='Last Name ']/../..//input";
        }

        public static string EmailTextBoxXpath()
        {
            return "//label[text()='Email Address ']/../..//input";
        }

        public static string EmailDetailTextBoxXpath()
        {
            return "//label[text()='Email ']/../..//input";
        }
        #endregion 

        #region Checkbox
        #endregion

        #region label
        public static string ContactsSpanXpath()
        {
            return "//span[text()='Contacts']";
        }

        public static string DetailPageLabelXpath()
        {
            return "//div[@class='col-md-6']//h2[contains(text(), 'Detail')]";  
        }
        #endregion 
    }
}
