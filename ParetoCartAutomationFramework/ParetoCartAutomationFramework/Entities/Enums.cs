﻿/**
 * @author: lmangoua
 * Date: 04/07/2018
 */

namespace ParetoCartAutomationFramework.Entities
{
    public class Enums
    {
        public enum BrowserType
        {
            Chrome,
            Firefox,
            IE,
            Safari
        }
    }
}
