﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * @author: lmangoua
 * Date: 09/07/2018
 * */

namespace ParetoCartAutomationFramework.PageObjects
{
    public static class ReusablePageObjects
    {
        #region Buttons   
        public static string AddContactButtonXpath()
        {
            return "//button/i[@class='fa-btn-icon ng-scope fa fa-plus']";
        }

        public static string SaveButtonXpath()
        {
            return "//button[text()='Save']";
        }

        public static string ReturnButtonXpath()
        {
            return "//button[text()=' Return']";
        }
        #endregion 

        #region Icons
        public static string HomeIconXpath()
        {
            return "//a[@id='37']//i";
        }

        public static string OrdersIconXpath()
        {
            return "//a[@id='36']//i";
        }

        public static string UsersIconXpath()
        {
            return "//a[@id='29']//i";
        }
        #endregion

        #region Label
        public static string ContactsPageLabelXpath()
        {
            return "//h2[@class='title_bar_title']//i";
        }
        #endregion

        #region Preloader
        public static string HelloPreloaderXpath()
        {
            return "//*[@id = 'hellopreloader']";
        }
        #endregion

        #region Validations
        public static string NameLabelValidationXpath(string name)
        {
            return "//span[contains(text(), '" + name + "')]";
        }

        public static string EmailLabelValidationXpath(string email)
        {
            return "//span[contains(text(), '" + email + "')]";
        }
        #endregion
    }
}
