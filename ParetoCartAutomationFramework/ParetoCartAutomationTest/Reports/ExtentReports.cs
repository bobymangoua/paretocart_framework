﻿using NUnit.Framework;
using RelevantCodes.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/** 
 * @author: lmangoua
 *  Date: 08/07/2018
 *  Description: Generate extent reports
 */

namespace ParetoCartAutomationTest.Reports
{
    [TestFixture]
    #region ExtentReport
    public class ExtentReport
    {
        public ExtentReports extent;
        public ExtentTest test;

        public string projectPath { get; private set; }

        [OneTimeSetUp]
        #region StartReport
        public void StartReport()
        {
            /*To Have the path of the current solution*/
            string path = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string actualPath = path.Substring(0, path.LastIndexOf("bin"));
            string projectPath = new Uri(actualPath).LocalPath; //Actual path of the solution

            /*Set up the TestReports folder in the solution to store extentReport*/
            string reportPath = projectPath + "TestReports\\extentReport.html";

            /*Tell the program to generate reports*/
            //extent = new ExtentReports(reportPath, true);
            extent = new ExtentReports(reportPath, true); //To create report for each execution set it to "false" & to overwrite the report after each run set it to "true" 

            /*To add some info to the report file*/
            extent.AddSystemInfo("Host Name", "Lionel")
                .AddSystemInfo("Environment", "QA")
                .AddSystemInfo("User Name", "Lionel Mangoua");

            /*Load configurations made in extent-config.xml file*/
            extent.LoadConfig(projectPath + "extent-report.xml");
        }
        #endregion

        [Test]
        #region PassingTest
        public void PassingTest()
        {
            test = extent.StartTest("Passing test");

            //Driver.Navigate().GoToUrl("http://www.google.com");

            try
            {
                Assert.IsTrue(true);
                test.Log(LogStatus.Pass, "Assertion passed");
            }
            catch (AssertionException)
            {
                Assert.IsTrue(false);
                test.Log(LogStatus.Fail, "Assertion failed");
                throw;
            }
        }
        #endregion

        [TearDown]
        #region GetResult
        public void GetResult()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stackTrace = "<pre>" + TestContext.CurrentContext.Result.StackTrace + "</pre>";
            var errorMessage = TestContext.CurrentContext.Result.Message;

            if (status == NUnit.Framework.Interfaces.TestStatus.Failed)
            {
                test.Log(LogStatus.Fail, status + errorMessage);
                test.Log(LogStatus.Info, "Snapshot below: " + test.AddScreenCapture(projectPath + "TestReports\\"));
            }

            extent.EndTest(test);
        }
        #endregion

        [OneTimeTearDown]
        #region EndReport
        public void EndReport()
        {
            extent.Flush();
            extent.Close();
        }
        #endregion
    }
    #endregion


}
