﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParetoCartAutomationFramework.PageObjects
{
    public static class LoginPageObjects
    {
        public static string EmailTextboxXpath()
        {
            return "//label[text()='Email ']/../..//input";
        }

        public static string PasswordTextboxXpath()
        {
            return "//label[text()='Password ']/../..//input";
        }

        public static string LoginButtonXpath()
        {
            return "//button[text()='Login']";
        }

        public static string LockIconXpath()
        {
            return "//i[@class='fa fa-lock']";
        }

        public static string LoginOptionXpath()
        {
            return "//a[text()='Login']";
        }

        public static string RegisterOptionXpath()
        {
            return "//a[text()='Register']";
        }
    }
}
