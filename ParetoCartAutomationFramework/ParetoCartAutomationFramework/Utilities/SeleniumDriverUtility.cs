﻿using ParetoCartAutomationFramework.Entities;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

/** 
 * @author: lmangoua
 *  Date: 04/07/2018
 *  Notes: Install "DotNetSeleniumExtras, Selenium Webdriver, selenium Support" packages in the Reference
 */

namespace ParetoCartAutomationFramework
{
    public class SeleniumDriverUtility
    {
        public static IWebDriver Driver { get; set; }
        public static int WaitTimeout = 6; //Should i make it private?
        private readonly Enums.BrowserType browserType;
        //public static InfoLog InfoLog;
        //public static ErrorLog Error;

        //We set up the URL
        #region BaseAddress
        public static string BaseAddress
        {
            get
            {
                return "http://10.11.1.56:83/";
            }
        }
        #endregion

        #region StartDriver
        public static void StartDriver()
        {
            var options = new ChromeOptions();
            options.AddArguments("disable-infobars"); //To remove message "chrome is being controlled by automated test software"
            options.AddArguments("--start-maximized"); //To maximize browser
            Driver = new ChromeDriver(@"D:\Softwares\Selenium Frameworks\Creating Automated Framework With Selenium\AutomationFreamework\packages", options);
            //Thread.Sleep(5000);
            SeleniumDriverUtility.Wait(TimeSpan.FromSeconds(5));
            Console.WriteLine("Successfully Started the Driver.");
        }
        #endregion

        //Initialize driver to run on Sauce Labs (Cloud Service for continuous testing)
        #region Initialize
        public static void Initialize()
        {
            var options = new ChromeOptions();
            options.AddArguments("disable-infobars"); //To remove message "chrome is being controlled by automated test software"
            options.AddArguments("--start-maximized"); //To maximize browser
            //Driver = new ChromeDriver(@"D:\Softwares\Selenium Frameworks\Creating Automated Framework With Selenium\AutomationFreamework\packages", options);
            ////Thread.Sleep(5000);
            //DesiredCapabilities capabilities = DesiredCapabilities.Firefox();
            //DesiredCapabilities capabilities = DesiredCapabilities.Chrome();
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities = options.ToCapabilities() as DesiredCapabilities;
            capabilities.SetCapability(CapabilityType.Platform, "Windows 8");
            capabilities.SetCapability(CapabilityType.Version, "");
            capabilities.SetCapability("name", "ParetoCart Web App");
            capabilities.SetCapability("username", "lionel");
            capabilities.SetCapability("accessKey", "67dd30a2-705e-4532-8ffc-13f78c493fd8");

            Driver = new RemoteWebDriver(new Uri("https://ondemand.saucelabs.com:80/wd/hub"), capabilities);

            SeleniumDriverUtility.Wait(TimeSpan.FromSeconds(5));
            Console.WriteLine("Successfully initialized the browser");
        }
        #endregion

        #region Close
        public static void Close()
        {
            Driver.Close();
        }
        #endregion

        #region Wait
        public static void Wait(TimeSpan timeSpan)
        {
            Thread.Sleep((int)(timeSpan.TotalSeconds * 1000));
        }
        #endregion

        #region Pause
        public static void Pause(int millisecondsWait)
        {
            try
            {
                Thread.Sleep(millisecondsWait);
            }
            catch (Exception e)
            {

            }
        }
        #endregion

        //Static methods
        #region WaitForElementByXpath
        public static void WaitForElementByXpath(string elementXpath)
        {
            bool elementFound = false;
            try
            {
                int WaitCount = 0;
                while (!elementFound && WaitCount < WaitTimeout)
                {
                    try
                    {
                        WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));

                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath(elementXpath)));
                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                        if (wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath(elementXpath))) != null)
                        {
                            elementFound = true; 
                            Console.WriteLine("Found element by Xpath: " + elementXpath);
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        elementFound = false;
                        Console.WriteLine("Did Not Find element by Xpath: " + elementXpath);
                    }

                    WaitCount++;
                }

                if (WaitCount == WaitTimeout)
                { 
                    GetElementFound1(elementFound);
                    Console.WriteLine("Reached TimeOut whilst waiting for element by Xpath: " + elementXpath);
                }
            }
            catch (Exception e)
            { 
                Console.WriteLine("\n[ERROR] Failed to wait for element by Xpath ---\n{0}", e.Message);
            }

            GetElementFound(elementFound);
        }

        private static bool GetElementFound1(bool elementFound)
        {
            return elementFound;
        }

        private static bool GetElementFound(bool elementFound)
        {
            return elementFound;
        }
        #endregion

        #region EnterTextByXpath
        public static void EnterTextByXpath(string elementXpath, string textToEnter)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToTypeIn = Driver.FindElement(By.XPath(elementXpath));
                elementToTypeIn.Clear();
                elementToTypeIn.SendKeys(textToEnter);

                Console.WriteLine("Entered text by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                //Insert code for to display comment "(" entering text - " + elementXpath + " - " + e.getMessage())"
                Console.WriteLine("\n[ERROR] Failed to enter text by Xpath  ---\n{0}", e.Message);
            }
        }
        #endregion

        #region HoverOverElementByXpath 
        public static void HoverOverElementByXpath(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);

                Actions hoverTo = new Actions(Driver);
                hoverTo.MoveToElement(Driver.FindElement(By.XPath(elementXpath)));
                hoverTo.Perform();

                Console.WriteLine("Hovered over element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                //Insert code for to display comment "("Failed to hover over element Xpath - " + e.getMessage())"
                Console.WriteLine("\n[ERROR] Failed to hover over element Xpath ---\n{0}", e.Message);
            }
        }
        #endregion

        #region HoverOverElementAndClickSubElementXpath
        public static void HoverOverElementAndClickSubElementXpath(string elementXpath, string subElementXpath)
        {

            try
            {
                WaitForElementByXpath(elementXpath);

                Actions hoverTo = new Actions(Driver);
                var menuHoverLink = Driver.FindElement(By.XPath(elementXpath));
                hoverTo.MoveToElement(menuHoverLink);

                var subLink = menuHoverLink.FindElement(By.XPath(subElementXpath));
                hoverTo.MoveToElement(subLink);
                hoverTo.Click();
                hoverTo.Perform();
                Console.WriteLine("Hovered over element and clicked sub element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                //Insert code for to display comment "("Failed to hover over element and click sub element by Xpath  - " + e.getMessage())"
                Console.WriteLine("\n[ERROR] Failed to hover over element and click sub element by Xpath ---\n{0}", e.Message);
            }
        }
        #endregion               

        #region ClickElementByXpath
        public static void ClickElementByXpath(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click();
                Console.WriteLine("Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            { 
                Console.WriteLine("\n[ERROR] Failed to click on element by Xpath ---\n{0}", e.Message);
            }
        }
        #endregion

        #region ClickElementByXpath2
        public static void ClickElementByXpath2(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath); 
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                //elementToClick.Click();
                Actions actions = new Actions(Driver);
                actions.MoveToElement(elementToClick).Click().Build().Perform(); 
                Console.WriteLine("Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                //Insert code for to display comment "(" Failed to click on element by Xpath - " + e.getMessage())"
                Console.WriteLine("\n[ERROR] Failed to click element by Xpath ---\n{0}", e.Message);
            }
        }
        #endregion

        #region ClickElementByXpath3
        public static void ClickElementByXpath3(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath(elementXpath)));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                //elementToClick.Click();
                Actions actions = new Actions(Driver);
                actions.MoveToElement(elementToClick).Click().Build().Perform();
                actions.MoveToElement(elementToClick).Click().Build().Perform();
                Console.WriteLine("Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                //Insert code for to display comment "(" Failed to click on element by Xpath - " + e.getMessage())"
                Console.WriteLine("\n[ERROR] Failed to click element by Xpath ---\n{0}", e.Message);
            }
        }
        #endregion

        #region ClickElementByXpathUsingAction
        public static void ClickElementByXpathUsingAction(string elementXpath)
        {
            try
            {
                Console.WriteLine("[INFO] Clicking element by Xpath: " + elementXpath);
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                //Thread.Sleep(200);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Id("hellopreloader")));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click(); 
                Console.WriteLine("Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            { 
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                Thread.Sleep(200);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Id("hellopreloader")));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click();
                Console.WriteLine("[ERROR] Failed to click element by Xpath: ", e.Message);
            }
        }
        #endregion

        #region ClickElementByXpathUsingAction2
        public static void ClickElementByXpathUsingAction2(string elementXpath)
        {
            try
            {
                Console.WriteLine("[INFO] Clicking element by Xpath: " + elementXpath);
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath(elementXpath)));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                //elementToClick.Click();
                //Thread.Sleep(3000);
                Actions actions = new Actions(Driver);
                actions.MoveToElement(elementToClick).Click().Perform(); 
                Console.WriteLine("Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                //Insert code for to display comment "(" Failed to click element by Xpath - " + e.getMessage())"
                Console.WriteLine("\nFailed to click element by Xpath ---\n{0}", e.Message);
            }
        }
        #endregion


        

    }
}
