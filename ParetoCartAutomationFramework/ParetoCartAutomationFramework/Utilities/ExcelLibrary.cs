﻿using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

/*
 * @author: lmangoua
 * Date: 17/08/18
 * */

namespace ParetoCartAutomationFramework.Utilities
{
    public class ExcelLibrary
    {
        #region ExcelToDataTable
        private static DataTable ExcelToDataTable(string fileName)
        {
            //open file and returns as Stream
            FileStream stream = File.Open(fileName, FileMode.Open, FileAccess.Read);

            //Createopenxmlreader via ExcelReaderFactory
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream); //.xlsx

            //Set the First Row as Column Name
            excelReader.IsFirstRowAsColumnNames = true;

            //Return as DataSet
            DataSet result = excelReader.AsDataSet();
             
            //Get all the Tables
            DataTableCollection table = result.Tables;

            //Store it in DataTable
            DataTable resultTable = table["Sheet1"]; //DataSet

            //return
            return resultTable;
        }
        #endregion

        #region PopulateInCollection
        static List<Datacollection> dataCol = new List<Datacollection>();

        public static void PopulateInCollection(string fileName) //Populating Data into Collections
        {
            DataTable table = ExcelToDataTable(fileName);

            //Iterate through the rows and columns of the Table
            for (int row = 1; row <= table.Rows.Count; row++)
            {
                for (int col = 0; col <= table.Columns.Count; col++)
                {
                    Datacollection dtTable = new Datacollection()
                    {
                        rowNumber = row,
                        colName = table.Columns[col].ColumnName,
                        colValue = table.Rows[row - 1][col].ToString()
                    };
                    //Add all the details for each row
                    dataCol.Add(dtTable);
                }
            }
        }
        #endregion

        #region ReadData
        public static string ReadData(int rowNumber, string columnName) //Reading data from Collection
        {
            try
            {
                //Retriving Data using LINQ to reduce much of iterations
                string data = (from colData in dataCol
                               where colData.colName == columnName && colData.rowNumber == rowNumber
                               select colData.colValue).SingleOrDefault();

                //var datas = dataCol.Where(x => x.colName == columnName && x.rowNumber == rowNumber).SingleOrDefault().colValue;
                return data.ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }
        #endregion

        #region GetData
        public static string GetData(int rowNumber, string parameterName) //Reading data from Collection
        {
            try
            {
                //Retriving Data using LINQ to reduce much of iterations
                string data = (from colData in dataCol
                               where colData.colName == parameterName && colData.rowNumber == rowNumber
                               select colData.colValue).SingleOrDefault();

                //var datas = dataCol.Where(x => x.colName == columnName && x.rowNumber == rowNumber).SingleOrDefault().colValue;
                string returnedValue = data.ToString();

                if (returnedValue == null)
                {
                    Console.WriteLine("\n[ERROR] Parameter ' " + parameterName + " ' not found ---\n{0}");
                    return returnedValue = "";
                }
                else
                {
                    return returnedValue;
                } 
            }
            catch (Exception e)
            {
                return null;
            }
        }
        #endregion

        #region Datacollection
        public class Datacollection //Custom Class
        {
            public int rowNumber { get; set; }
            public string colName { get; set; }
            public string colValue { get; set; }
        }
        #endregion
    }
}
